//-----------------------------------------------------------------------------
// Copyright (c) 2012 GarageGames, LLC
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// Updated Gideon Model done in 3DS Max with Stock Torque Animations
//-----------------------------------------------------------------------------

singleton TSShapeConstructor(GideonReduxDts)
{
   baseShape = "./gideon.dts";
   sequence0 = "./player_root.dsq root";
   sequence1 = "./player_back.dsq back";
   sequence2 = "./player_side.dsq side";
   sequence3 = "./player_lookde.dsq look";
   sequence4 = "./player_headlook.dsq headLook";
   sequence5 = "./player_fall.dsq fall";
   sequence6 = "./player_land.dsq land";
   sequence7 = "./player_jump.dsq jump";
   sequence8 = "./player_diehead.dsq death1";
   sequence9 = "./player_diechest.dsq death2";
   sequence10 = "./player_dieback.dsq death3";
   sequence11 = "./player_diesidelf.dsq death4";
   sequence12 = "./player_diesidert.dsq death5";
   sequence13 = "./player_dieleftleg.dsq death6";
   sequence14 = "./player_dierightleg.dsq death7";
   sequence15 = "./player_dieslump.dsq death8";
   sequence16 = "./player_dieknees.dsq death9";
   sequence17 = "./player_dieforward.dsq death10";  
   sequence18 = "./player_diespin.dsq death11";
   sequence19 = "./player_looksn.dsq looksn";
   sequence20 = "./player_lookms.dsq lookms";
   sequence21 = "./player_root.dsq scoutroot";
   sequence22 = "./player_headside.dsq headside";
   sequence23 = "./player_recoil.dsq light_recoil";
   sequence24 = "./player_crouchroot.dsq sitting";
   sequence25 = "./player_celsalute.dsq celsalute";
   sequence26 = "./player_celwave.dsq celwave";
   sequence27 = "./player_standjump.dsq standjump";
   sequence28 = "./player_looknw.dsq looknw";
   sequence29 = "./player_celcheck.dsq celcheck";
   sequence30 = "./player_celimp.dsq celimp";
   sequence31 = "./player_celrocky.dsq celrocky";
   sequence32 = "./player_celtaunt.dsq taunt";
   sequence33 = "./player_celdance.dsq celdance";
   sequence34 = "./player_celflex.dsq celflex";
   sequence35 = "./player_crouchroot.dsq crouch_root";
   sequence36 = "./player_crouchback.dsq crouch_back";
   sequence37 = "./player_crouchforward.dsq crouch_forward";
   sequence38 = "./player_crouchside.dsq crouch_side";
   sequence39 = "./player_swimroot.dsq swim_root";
   sequence40 = "./player_swimback.dsq swim_backward";
   sequence41 = "./player_swimforward.dsq swim_forward";
   sequence42 = "./player_swimsidelf.dsq swim_left";
   sequence43 = "./player_swimsiderg.dsq swim_right";
};         