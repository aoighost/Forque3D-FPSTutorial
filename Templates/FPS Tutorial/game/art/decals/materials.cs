//-----------------------------------------------------------------------------
// Copyright (c) 2012 GarageGames, LLC
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.
//-----------------------------------------------------------------------------

singleton Material(DECAL_scorch)
{
   baseTex[0] = "art/decals/scorch_decal";

   vertColor[0] = true;
   translucent = true;
   translucentBlendOp = LerpAlpha;
   translucentZWrite = true;
};

singleton Material(DECAL_RocketEXP)
{
   baseTex[0] = "art/decals/rBlast";

   vertColor[0] = true;
   translucent = true;
   translucentBlendOp = LerpAlpha;
   translucentZWrite = true;
};

singleton Material(DECAL_bulletHole)
{
   baseTex[0] = "art/decals/Bullet Holes/BulletHole_Walls.dds";

   vertColor[0] = true;
   translucent = true;
   translucentBlendOp = LerpAlpha;
   translucentZWrite = true;
};

singleton Material(DECAL_dirt01)
{
   baseTex[0] = "art/decals/dirt/dirt_01";

   vertColor[0] = true;
   translucent = true;
   translucentBlendOp = LerpAlpha;
   translucentZWrite = true;
};

singleton Material(DECAL_wallDamage01)
{
   baseTex[0] = "art/decals/wall_damage/wallDmg01";

   vertColor[0] = true;
   translucent = true;
   translucentBlendOp = LerpAlpha;
   translucentZWrite = true;
};

singleton Material(DECAL_wallDamage02)
{
   baseTex[0] = "art/decals/wall_damage/wallDmg02";

   vertColor[0] = true;
   translucent = true;
   translucentBlendOp = LerpAlpha;
   translucentZWrite = true;
};

singleton Material(DECAL_wallDamage03)
{
   baseTex[0] = "art/decals/wall_damage/wallDmg03";

   vertColor[0] = true;
   translucent = true;
   translucentBlendOp = LerpAlpha;
   translucentZWrite = true;
   alphaTest = false;
};

singleton Material(DECAL_wallDamage04)
{
   baseTex[0] = "art/decals/wall_damage/wallDmg04";

   vertColor[0] = true;
   translucent = true;
   translucentBlendOp = LerpAlpha;
   translucentZWrite = true;
};

singleton Material(DECAL_Graf01)
{
   baseTex[0] = "art/decals/Graffiti/graf_01";

   vertColor[0] = true;
   translucent = true;
   translucentBlendOp = LerpAlpha;
   translucentZWrite = true;
};

singleton Material(DECAL_Graf02)
{
   baseTex[0] = "art/decals/Graffiti/graf_02";

   vertColor[0] = true;
   translucent = true;
   translucentBlendOp = LerpAlpha;
   translucentZWrite = true;
};

singleton Material(DECAL_Graf03)
{
   baseTex[0] = "art/decals/Graffiti/graf_03";

   vertColor[0] = true;
   translucent = true;
   translucentBlendOp = LerpAlpha;
   translucentZWrite = true;
};

singleton Material(DECAL_Graf04)
{
   baseTex[0] = "art/decals/Graffiti/graf_04";

   vertColor[0] = true;
   translucent = true;
   translucentBlendOp = LerpAlpha;
   translucentZWrite = true;
};

singleton Material(DECAL_Graf05)
{
   baseTex[0] = "art/decals/Graffiti/graf_05";

   vertColor[0] = true;
   translucent = true;
   translucentBlendOp = LerpAlpha;
   translucentZWrite = true;
};

singleton Material(DECAL_Graf06)
{
   baseTex[0] = "art/decals/Graffiti/graf_06";

   vertColor[0] = true;
   translucent = true;
   translucentBlendOp = LerpAlpha;
   translucentZWrite = true;
};

singleton Material(DECAL_Graf07)
{
   baseTex[0] = "art/decals/Graffiti/graf_07";

   vertColor[0] = true;
   translucent = true;
   translucentBlendOp = LerpAlpha;
   translucentZWrite = true;
};

singleton Material(DECAL_Graf08)
{
   baseTex[0] = "art/decals/Graffiti/graf_08";

   vertColor[0] = true;
   translucent = true;
   translucentBlendOp = LerpAlpha;
   translucentZWrite = true;
};

singleton Material(DECAL_Graf09)
{
   baseTex[0] = "art/decals/Graffiti/graf_09";

   vertColor[0] = true;
   translucent = true;
   translucentBlendOp = LerpAlpha;
   translucentZWrite = true;
};

singleton Material(DECAL_dirt02)
{
   baseTex[0] = "art/decals/dirt/dirt_02";

   vertColor[0] = true;
   translucent = true;
   translucentBlendOp = LerpAlpha;
   translucentZWrite = true;
};

singleton Material(DECAL_defaultblobshadow)
{
   baseTex[0] = "art/decals/defaultblobshadow";

   vertColor[0] = true;
   translucent = true;
   translucentBlendOp = LerpAlpha;
   translucentZWrite = true;
};

singleton Material(DECAL_leaves)
{
   diffuseMap[0] = "art/decals/Leafs/Leafs_SpreadOut";

   vertColor[0] = true;
   translucent = true;
   translucentBlendOp = LerpAlpha;
   translucentZWrite = true;
};

singleton Material(DECAL_poster01)
{
   baseTex[0] = "art/decals/posters_01";

   vertColor[0] = true;
   translucent = true;
   translucentBlendOp = LerpAlpha;
   translucentZWrite = true;
};

singleton Material(DECAL_poster02)
{
   baseTex[0] = "art/decals/posters_02";

   vertColor[0] = true;
   translucent = true;
   translucentBlendOp = LerpAlpha;
   translucentZWrite = true;
};

singleton Material(DECAL_poster03)
{
   baseTex[0] = "art/decals/posters_03";

   vertColor[0] = true;
   translucent = true;
   translucentBlendOp = LerpAlpha;
   translucentZWrite = true;
};

singleton Material(DECAL_poster04)
{
   baseTex[0] = "art/decals/posters_04";

   vertColor[0] = true;
   translucent = true;
   translucentBlendOp = LerpAlpha;
   translucentZWrite = true;
};

singleton Material(DECAL_poster05)
{
   baseTex[0] = "art/decals/posters_05";

   vertColor[0] = true;
   translucent = true;
   translucentBlendOp = LerpAlpha;
   translucentZWrite = true;
};

singleton Material(DECAL_poster06)
{
   baseTex[0] = "art/decals/deli_posters/posters_dumpling";

   vertColor[0] = true;
   translucent = true;
   translucentBlendOp = LerpAlpha;
   translucentZWrite = true;
};

singleton Material(DECAL_poster07)
{
   baseTex[0] = "art/decals/deli_posters/posters_milk";

   vertColor[0] = true;
   translucent = true;
   translucentBlendOp = LerpAlpha;
   translucentZWrite = true;
};

singleton Material(DECAL_crack01)
{
   baseTex[0] = "art/decals/cracks/cracks_01";

   vertColor[0] = true;
   translucent = true;
   translucentBlendOp = LerpAlpha;
   translucentZWrite = true;
};

singleton Material(DECAL_crack02)
{
   baseTex[0] = "art/decals/cracks/cracks_02";

   vertColor[0] = true;
   translucent = true;
   translucentBlendOp = LerpAlpha;
   translucentZWrite = true;
};

singleton Material(DECAL_crack03)
{
   baseTex[0] = "art/decals/cracks/cracks_03";

   vertColor[0] = true;
   translucent = true;
   translucentBlendOp = LerpAlpha;
   translucentZWrite = true;

};

singleton Material(DECAL_blossoms)
{
   baseTex[0] = "art/decals/Leafs/cherryBlossoms";

   vertColor[0] = true;
   translucent = true;
   translucentBlendOp = LerpAlpha;
   translucentZWrite = true;

};

singleton Material(DECAL_puddle)
{
	baseTex[0] = "art/decals/puddle";
   cubemap = "refDaySkyCubemap";

   vertColor[0] = true;
   translucent = true;
   translucentBlendOp = LerpAlpha;
   translucentZWrite = true;
};

singleton Material(DECAL_trashPile)
{
	baseTex[0] = "art/decals/Trash/TrashHeaps_COLOR_02";

   vertColor[0] = true;
   translucent = true;
   translucentBlendOp = LerpAlpha;
   translucentZWrite = true;
};

singleton Material(DECAL_newsPapers)
{
	baseTex[0] = "";

   vertColor[0] = true;
   translucent = true;
   translucentBlendOp = LerpAlpha;
   translucentZWrite = true;
   mapTo = "NewsP_COL_LowSaturation";
   diffuseColor[0] = "0.513726 0.513726 0.513726 1";
   diffuseMap[0] = "art/decals/Papers/newspapers/NewsP_COL_LowSaturation";
};

singleton Material(DECAL_Papers)
{
   baseTex[0] = "";

   vertColor[0] = true;
   translucent = true;
   translucentBlendOp = LerpAlpha;
   translucentZWrite = true;
   mapTo = "shreds_COL";
   diffuseColor[0] = "0.541176 0.541176 0.541176 1";
   diffuseMap[0] = "art/decals/Papers/Paper shreds/shreds_COL";
};

singleton Material(DECAL_carpets)
{
	baseTex[0] = "art/decals/carpets/Carpets01.dds";

   vertColor[0] = true;
   translucent = true;
   translucentBlendOp = LerpAlpha;
   translucentZWrite = true;
};

